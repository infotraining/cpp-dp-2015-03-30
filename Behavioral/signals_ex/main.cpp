#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

class Printer
{
public:
    void print(const string& msg, double arg)
    {
        cout << msg << arg << endl;
    }
};

class Logger
{
public:
    void log(const string& msg)
    {
        cout << "Log: " << msg << endl;
    }
};

void save_to_db(double temp)
{
    cout << "Saving to db: " << temp << endl;
}

class TemperatureMonitor
{
public:
    // zdefiniować typ sygnału TemperatureChangedSignal
    typedef boost::signals2::signal<void(double)> TemperatureChanedSignal;

    // zdefiniować typ slotów dla sygnału
    typedef TemperatureChanedSignal::slot_type TemperatureChangedSlot;

    TemperatureMonitor() : temp_(0.0) {}

    // implementacja podłączenia slotu do sygnału
    boost::signals2::connection attach(const TemperatureChangedSlot& slot)
    {
        return temp_changed_.connect(slot);
    }

    void set_temp(double temp)
    {
        if (temp_ != temp)
        {
            temp_ = temp;
            // emisja sygnalu
            temp_changed_(temp_);
        }
    }

private:
    // sygnał zmiany temperatury
    TemperatureChanedSignal temp_changed_;
    double temp_;
};


int main()
{
    using namespace boost::signals2;

    Printer printer;
    TemperatureMonitor monitor;
    Logger logger;

    // podłączenie Printera
    monitor.attach([&printer](double temp) { printer.print("New temp: ", temp);});

    // podłączenie Loggera
    monitor.attach([&logger](double temp) { logger.log("Logging new temp " + to_string(temp));});

    monitor.set_temp(24.0);
    monitor.set_temp(45.0);

    {
        boost::signals2::scoped_connection conn { monitor.attach(&save_to_db) };

        monitor.set_temp(124.0);
        monitor.set_temp(245.0);
    }

    monitor.set_temp(45.0);

    {
        auto shared_printer = boost::make_shared<Printer>();

        monitor.attach(TemperatureMonitor::TemperatureChangedSlot(
                           &Printer::print, shared_printer.get(), "Shared printer: ", _1)
                                .track(shared_printer));

        monitor.set_temp(400.0);
    }

    monitor.set_temp(45.0);
}
