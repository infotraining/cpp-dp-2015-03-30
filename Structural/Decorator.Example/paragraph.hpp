#ifndef PARAGRAPH_H_
#define PARAGRAPH_H_

#include <iostream>
#include <string>
#include <memory>

class IParagraph
{
public:
    virtual std::string getHTML() const = 0;
    virtual ~IParagraph() = default;
};

class Paragraph : public IParagraph
{
	std::string text_;
public:
	Paragraph(const std::string& initial_text) : text_(initial_text)
	{
	}
	
    virtual ~Paragraph() = default;
	
    virtual std::string getHTML() const override
	{
		return text_;
	}
};

class BoldParagraph : public IParagraph
{
protected:
    std::shared_ptr<IParagraph> wrapped_paragraph_;
public:
    BoldParagraph(std::shared_ptr<IParagraph> paragraph) : wrapped_paragraph_(paragraph)
	{
	}

    std::string getHTML() const override
	{
		return "<b>" + wrapped_paragraph_->getHTML() + "</b>";
	}
};

class ItalicParagraph : public IParagraph
{
protected:
    std::shared_ptr<IParagraph> wrapped_paragraph_;
public:
    ItalicParagraph(std::shared_ptr<IParagraph> paragraph) : wrapped_paragraph_(paragraph)
	{
	}

    std::string getHTML() const override
	{
		return "<i>" + wrapped_paragraph_->getHTML() + "</i>";
	}
};

#endif /*PARAGRAPH_H_*/
