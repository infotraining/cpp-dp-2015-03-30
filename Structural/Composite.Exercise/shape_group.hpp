#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include <memory>
#include "shape.hpp"
#include "clone_factory.hpp"

namespace Drawing
{
    using ShapePtr = std::shared_ptr<Shape>;

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
    class ShapeGroup : public Shape
    {
        std::vector<ShapePtr> shapes_;

        // Shape interface
    public:
        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            for(const auto& s : source.shapes_)
                shapes_.emplace_back(s->clone());
        }

        ShapeGroup(ShapeGroup&& source) = default;

        ShapeGroup& operator=(const ShapeGroup& source)
        {
            ShapeGroup temp = source;

            swap(temp);

            return *this;
        }

        ShapeGroup& operator=(ShapeGroup&& source) = default;

        void swap(ShapeGroup& sg)
        {
            shapes_.swap(sg.shapes_);
        }

        void draw() const
        {
            for(const auto& s : shapes_)
                s->draw();
        }

        void move(int dx, int dy)
        {
            for(const auto& s : shapes_)
                s->move(dx, dy);
        }

        void read(std::istream& in)
        {
            int count;
            in >> count;

            for(int i = 0; i < count; ++i)
            {
                std::string type_identifier;

                in >> type_identifier;

                auto shape = ShapeFactory::instance().create(type_identifier);
                shape->read(in);
                shapes_.emplace_back(shape);
            }
        }

        void write(std::ostream& out)
        {
            out << "ShapeGroup " << shapes_.size() << std::endl;

            for(const auto& s : shapes_)
                s->write(out);
        }

        Shape* clone() const
        {
            return new ShapeGroup(*this);
        }
    };
}

#endif /*SHAPE_GROUP_HPP_*/
