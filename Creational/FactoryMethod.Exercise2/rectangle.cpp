#include "rectangle.hpp"
#include "shape_factory.hpp"

namespace
{
    bool is_registered
        = Drawing::ShapeFactory::instance()
            .register_creator("Rectangle",
                              []{ return new Drawing::Rectangle(); });
}
