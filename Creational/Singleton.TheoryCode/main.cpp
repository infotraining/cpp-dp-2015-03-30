#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    cout << "Start" << endl;

    Multithreading::Singleton::instance().do_something();

    Multithreading::Singleton& singleObject = Multithreading::Singleton::instance();
	singleObject.do_something();
}
