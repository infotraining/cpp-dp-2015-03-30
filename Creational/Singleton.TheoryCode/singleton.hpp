#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <mutex>
#include <atomic>
#include <memory>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{
        if (!instance_.load())
        {
            std::lock_guard<std::mutex> lk {mtx_};
            if (!instance_.load())
            {
                instance_ = new Singleton();
            }
        }

		return *instance_;
	}

	void do_something();

private:
    static std::mutex mtx_;
    static std::atomic<Singleton*> instance_;  // uniqueInstance
	
	Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 

	~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
	{ 
		std::cout << "Singleton has been destroyed!" << std::endl;
	} 
};

std::mutex Singleton::mtx_;
std::atomic<Singleton*> Singleton::instance_ {};

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}

namespace Meyers
{
    class Singleton
    {
    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        static Singleton& instance()
        {
            static Singleton unique_instance_;

            return unique_instance_;
        }

        void do_something();

    private:
        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }
    };

    void Singleton::do_something()
    {
        std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
    }
}

namespace Multithreading
{
    class Singleton
    {
    public:
        Singleton(const Singleton&) = delete;
        Singleton& operator=(const Singleton&) = delete;

        static Singleton& instance()
        {
            std::call_once(init_flag_, []{ instance_.reset(new Singleton());});

            return *instance_;
        }

        void do_something();

        ~Singleton() // prywatny destruktor chroni przed wywolaniem delete dla adresu instancji
        {
            std::cout << "Singleton has been destroyed!" << std::endl;
        }

    private:
        static std::once_flag init_flag_;
        static std::unique_ptr<Singleton> instance_;

        Singleton() // uniemozliwienie klientom tworzenie nowych singletonow
        {
            std::cout << "Constructor of singleton" << std::endl;
        }
    };

    std::once_flag Singleton::init_flag_;
    std::unique_ptr<Singleton> Singleton::instance_ {nullptr};

    void Singleton::do_something()
    {
        std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
    }
}


#endif /*SINGLETON_HPP_*/
